<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Icriq extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'services'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];
    
    /*
    * table name
    *
    * @var string
    */
   protected $table = 'icriqs';
    

  
}
