<?php

namespace App;

use App\Models\Icriq;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;

class IcriqExport implements FromCollection
{
    public function collection()
    {
        $collection = collect();
        $icriqs = Icriq::all();
        foreach ($icriqs as $model) {
            $item = new Icriq();
            $item->name = $model->name;
            $services = json_decode($model->services);
            foreach ($services as $key => $value) {
                $item->$key = 
                [ $key => $value ];
            }
            $collection->add($item); 
        }
        return  $collection; 
    }
}