<?php

namespace App\Http\Controllers;

use App\IcriqExport;
use App\Models\Icriq;
use Zttp\Zttp;
use PHPHtmlParser\Dom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;



class IcriqController extends Controller
{
    /**
     * this function srap a file  diractly
     */
    public function create(Request $request)
    {
        $request->validate([
            'file' => 'required'
        ]);
 
       // $path = '/home/wahid/Downloads/icriq test file/ADN.html';
        $path = $request->file('file')->getRealPath();
        // create a dom object
        $dom = new Dom;
        $dom->loadFromFile($path);
        $title = $dom->find('title')[0];
        $title =  $title->text;
        // company name
        $b = $dom->find('b');
        $name  = $b->text;
        // get the main table of the page
        $tables = $dom->find('table');
        $childs_tables  = $tables[0]->find('table');
        // get the first table with the description section
        // and the export section 

        $childs_tables1  = $childs_tables[0]->find('table');
        $childs_tables2  = $childs_tables1[1]->find('table');
        $tr = $childs_tables2[1]->find('tr');
        $td = $tr[1]->find('td');
        
        // for each  tr create attributs and create 
        // ligne of this column 

        $services = array();

        $trs = $childs_tables1[1]->find('tr');
        if (count($trs) > 0) {
            foreach ($trs as $tr) {
                $bs = $tr->find('b');
                // if the Tr contain a b then there is an iformation to scrap
                if (count($bs) > 0) {
                    foreach ($bs as $b) {
                        $services_name = [];
                        $tds = $tr->find('td');
                        foreach ($tds as $td) {
                            if ($td->text !== '') {
                                array_push($services_name, $td->text);
                            }
                            $tables = $td->find('table');
                            if (count($tables) > 0) {
                                foreach ($tables as $table) {
                                  
                                    $trsTable = $table->find('tr');
                                  
                                    foreach ($trsTable as $trTable) {
                                        $tdsTrTable =  $trTable->find('td');
                                        foreach ($tdsTrTable as $tdTrTable) {
                                            // inside a table test of table 
                                            $insideTbales = $tdTrTable->find('table');
                                            $services_content = array();
                                            foreach ($insideTbales as $insidetable) {
                                                 $insidetrsTable = $insidetable->find('tr');
                                                foreach ($insidetrsTable as $insidetrTable) {
                                                    $insidetdsTrTable =  $insidetrTable->find('td');

                                                    foreach ($insidetdsTrTable as $insidetdTrTable) {

                                                        $insidTablestdTr = $insidetdTrTable->find('table');
                                                        foreach ($insidTablestdTr as $insidTabletdTr) {
                                                            $insidtdsTabletdTr = $insidTabletdTr->find('td');
                                                            foreach ($insidtdsTabletdTr as $insidtdTabletdTr) {
                                                                if (
                                                                    $insidtdTabletdTr->text !== '' && $insidtdTabletdTr->text !== '  '
                                                                    && $insidtdTabletdTr->text  !== '&nbsp;'
                                                                ) {
                                                                    array_push($services_content, $insidtdTabletdTr->text);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if ($tdTrTable->text !== '' && $tdTrTable->text !== '  '  && $tdTrTable->text  !== '&nbsp;') {
                                                array_push($services_name, $tdTrTable->text);
                                                if (count($services_content) > 0) {
                                                    array_push($services_name[$tdTrTable->text], $services_content);
                                                }
                                            } else {

                                                if (count($services_content) > 0) {
                                                    array_push($services_name, $services_content);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (count($services_name) > 0) {
                            $services[$b->text] = $services_name;
                        }
                    }
                }
            }
        }



        $trs = $childs_tables2[2]->find('tr');

        foreach ($trs as $tr) {
            $bs = $tr->find('b');
            foreach ($bs as $b) {
                if (count($bs) > 0) {
                    // //echo 'count of bb =='.count($b);
                    //$services[$b[0]->text] = $b[0]->text;
                    $sericeChilds = [];
                    $tables = $tr->find('table');
                    if (count($tables) > 0) {
                        foreach ($tables as $table) {
                            $tableChilds =  $table->find('table');
                            if (count($tableChilds) > 0) {
                                foreach ($table as $tableChild) {
                                    $trChilds = $tableChild->find('tr');
                                    if (count($trChilds) > 0) {
                                        # code...
                                        foreach ($trChilds as $trChild) {
                                            $tdsChils = $trChild->find('td');

                                            if ($tdsChils[1]->text !== '' && $tdsChils[1]->text !== '  ' && $tdsChils->text  !== '&nbsp;') {
                                                array_push($sericeChilds, $tdsChils[1]->text);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //test the td 
                    $tds = $tr->find('td');
                    if (count($tds) > 0) {
                        foreach ($tds as $td) {
                            if ($td->text !== '' && $td->text !== '  ' && $td->text  !== '&nbsp;') {
                                // //echo $td->text; 
                                array_push($sericeChilds, $td->text);
                            }
                        }
                    }
                    if (count($sericeChilds) > 0) {
                        $services[$b->text] = $sericeChilds;
                    }
                }
            }
        }
        // create the icriq object 
        $icriq = new Icriq();
        $icriq->name = $name; 
        $icriq->services =json_encode($services);
        $icriq->save(); 
        return Excel::download(new IcriqExport, 'icriq.xlsx');
    }

    public function data()
    {
        $path = '/home/wahid/Downloads/icriq test file/ADN.html';
        //$path = $request->file('file')->getRealPath();
        // create a dom object
        $dom = new Dom;
        $dom->loadFromFile($path);
        $title = $dom->find('title')[0];
        $title =  $title->text;
        // company name
        $b = $dom->find('b');
        $name  = $b->text;
        // get the main table of the page
        $tables = $dom->find('table');
        $childs_tables  = $tables[0]->find('table');
        // get the first table with the description section
        // and the export section 

        $childs_tables1  = $childs_tables[0]->find('table');
        $childs_tables2  = $childs_tables1[1]->find('table');
        $tr = $childs_tables2[1]->find('tr');
        $td = $tr[1]->find('td');
        
        // for each  tr create attributs and create 
        // ligne of this column 

        $services = array();

        $trs = $childs_tables1[1]->find('tr');
        if (count($trs) > 0) {
            foreach ($trs as $tr) {
                $bs = $tr->find('b');
                // if the Tr contain a b then there is an iformation to scrap
                if (count($bs) > 0) {
                    foreach ($bs as $b) {
                        $services_name = [];
                        $tds = $tr->find('td');
                        foreach ($tds as $td) {
                            if ($td->text !== '') {
                                array_push($services_name, $td->text);
                            }
                            $tables = $td->find('table');
                            if (count($tables) > 0) {
                                foreach ($tables as $table) {
                                  
                                    $trsTable = $table->find('tr');
                                  
                                    foreach ($trsTable as $trTable) {
                                        $tdsTrTable =  $trTable->find('td');
                                        foreach ($tdsTrTable as $tdTrTable) {
                                            // inside a table test of table 
                                            $insideTbales = $tdTrTable->find('table');
                                            $services_content = array();
                                            foreach ($insideTbales as $insidetable) {
                                                 $insidetrsTable = $insidetable->find('tr');
                                                foreach ($insidetrsTable as $insidetrTable) {
                                                    $insidetdsTrTable =  $insidetrTable->find('td');

                                                    $service_mulieu = array(); 
                                                    foreach ($insidetdsTrTable as $insidetdTrTable) {

                                                        $insidTablestdTr = $insidetdTrTable->find('table');
                                                        foreach ($insidTablestdTr as $insidTabletdTr) {
                                                            $insidtdsTabletdTr = $insidTabletdTr->find('td');
                                                            foreach ($insidtdsTabletdTr as $insidtdTabletdTr) {
                                                                if (
                                                                    $insidtdTabletdTr->text !== '' && $insidtdTabletdTr->text !== '  '
                                                                    && $insidtdTabletdTr->text  !== '&nbsp;'
                                                                ) {
                                                                    array_push($service_mulieu, $insidtdTabletdTr->text);
                                                                }
                                                            }
                                                        }
                                                        if (count($service_mulieu)> 0) {
                                                            $services_content = $service_mulieu;
                                                            // array_push($services_content, $service_mulieu);
                                                            $service_mulieu = array(); 
                                                            # code...
                                                        }
                                                    }
                                                    
                                                }
                                            }
                                            if ($tdTrTable->text !== '' && $tdTrTable->text !== '  '  && $tdTrTable->text  !== '&nbsp;') {
                                                array_push($services_name, $tdTrTable->text);
                                                if (count($services_content) > 0) {
                                                    array_push($services_name[$tdTrTable->text], $services_content);
                                                    $services_content= array();
                                                }
                                            } else {

                                                if (count($services_content) > 0) {
                                                    array_push($services_name, $services_content);
                                                    $services_content= array();

                                                }
                                            }
                                        }
                                    }
                                }
                                if (count($services_name) > 0) {
                                    $services[$b->text] = $services_name;
                                    $services_name = array(); 
                                }
                            }
                        }

                        // if (count($services_name) > 0) {
                        //     $services[$b->text] = $services_name;
                        // }
                    }
                }
            }
        }



        $trs = $childs_tables2[2]->find('tr');

        foreach ($trs as $tr) {
            $bs = $tr->find('b');
            foreach ($bs as $b) {
                if (count($bs) > 0) {
                    // //echo 'count of bb =='.count($b);
                    //$services[$b[0]->text] = $b[0]->text;
                    $sericeChilds = [];
                    $tables = $tr->find('table');
                    if (count($tables) > 0) {
                        foreach ($tables as $table) {
                            $tableChilds =  $table->find('table');
                            if (count($tableChilds) > 0) {
                                foreach ($table as $tableChild) {
                                    $trChilds = $tableChild->find('tr');
                                    if (count($trChilds) > 0) {
                                        # code...
                                        foreach ($trChilds as $trChild) {
                                            $tdsChils = $trChild->find('td');

                                            if ($tdsChils[1]->text !== '' && $tdsChils[1]->text !== '  ' && $tdsChils->text  !== '&nbsp;') {
                                                array_push($sericeChilds, $tdsChils[1]->text);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //test the td 
                    $tds = $tr->find('td');
                    if (count($tds) > 0) {
                        foreach ($tds as $td) {
                            if ($td->text !== '' && $td->text !== '  ' && $td->text  !== '&nbsp;') {
                                // //echo $td->text; 
                                array_push($sericeChilds, $td->text);
                            }
                        }
                    }
                    if (count($sericeChilds) > 0) {
                        $services[$b->text] = $sericeChilds;
                    }
                }
            }
        }
        // create the icriq object 
        $icriq = new Icriq();
        $icriq->name = $name; 
        return $services ; 
        $icriq->services =json_encode($services);
        return  $icriq;
        $icriq->save(); 
        return Excel::download(new IcriqExport, 'icriq.xlsx');
    }



}
