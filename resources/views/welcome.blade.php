<html lang="en">

<head>
    <title>Laravel 5.6 Import Export to Excel and csv Example - ItSolutionStuff.com</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1>Laravel Import  HTML Export to Excel </h1>
            </div>
            <div class="panel-body">

                <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;"
                    action="{{ url('/') }}" class="form-horizontal" method="post"
                    enctype="multipart/form-data">
                    @csrf

                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    @if (Session::has('success'))
                    <div class="alert alert-success">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                        <p>{{ Session::get('success') }}</p>
                    </div>
                    @endif

                    <h3> import an HTML file</h3>
                    <input type="file" name="file" />
                    <button class="btn btn-primary">Import File</button>
                </form>



            </div>
        </div>
    </div>
</body>

</html>